//console.log("Hello");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInfo() {
		let name = prompt ("What is your Name?");
		let age = prompt ("How old are you? ");
		let address = prompt ("Where do you live? ");

		console.log("Hello, " + name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
		alert("Thank you for your input!");
	}
	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topFavoriteMusic() {
		var favMusic1 = "Blackpink";
		var favMusic2 = "Enhypen";
		var favMusic3 = "New Jeans";
		var favMusic4 = "Lany";
		var favMusic5 = "Dua Lipa";
	
		console.log("1. " + favMusic1);
		console.log("2. " + favMusic2);
		console.log("3. " + favMusic3);
		console.log("4. " + favMusic4);
		console.log("5. " + favMusic5);
	}
	topFavoriteMusic();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topFavoriteMovies() {
		var favMovie1 = "The God Father";
		var favMovie2 = "The God Father, Part II";
		var favMovie3 = "Shawshank Redemption";
		var favMovie4 = "To Kill A Mockingbird";
		var favMovie5 = "Psycho";
	
		console.log("1. " + favMovie1);
		console.log("Rotten Tomatoes Rating: 97%");

		console.log("2. " + favMovie2);
		console.log("Rotten Tomatoes Rating: 96%");

		console.log("3. " + favMovie3);
		console.log("Rotten Tomatoes Rating: 91%");

		console.log("4. " + favMovie4);
		console.log("Rotten Tomatoes Rating: 93%");

		console.log("5. " + favMovie5);
		console.log("Rotten Tomatoes Rating: 96%");
	}
	topFavoriteMovies();
	
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:");
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	}
	printFriends();

